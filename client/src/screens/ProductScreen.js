import React from 'react';
import { Link } from 'react-router-dom';

import products from '../products';

const ProductScreen = ({ match }) => {

  // To get single from the products array 
  // (remember products array is a temporary file for now)
  const product = products.find((p) => p._id === match.params.id)

  return (
    <>
      <Link to='/' 
        className='waves-effect waves-light btn-small' 
        style={{marginTop: '1.2em', marginBottom: '1.2em'}}
      >
        Go Back
      </Link>

      
        <div className='row'>

          <div className='col s12 m6'>
            <div className='container'>
              <img className='responsive-img' src={product.image} alt={product.name} />
            </div>
          </div>

          <ul className='col s12 m3'>
            <li>
              <h5>{product.name}</h5>
            </li>

            <p>
              {/* ToDo: Add custom css for divider line */}
              <li className='list-divider'>
                Price: £{product.price}
              </li>
            </p>

            <li>
              Description: {product.description}
            </li>
          </ul>

          <div className='col s12 m3'>
            <div className='card'>
              <ul>
                <li>
                  <div className='row'>
                    <div className='col s12 m6'>Price:</div>
                    <div className='col s12 m6'><strong>£{product.price}</strong></div>
                  </div>
                </li>

                <li>
                  <div className='row'>
                    <div className='col s12 m6'>Status:</div>
                    <div className='col s12 m6'>{product.countInStock > 0 ? 'In Stock' : 'Out of Stock'}</div>
                  </div>
                </li>

                <li className='col s12'>
                  
                    <button className='btn waves-effect waves-light' type='submit'>Add To Cart</button>
                  
                </li>
                
              </ul>

            </div>
          </div>
          
          
        </div>
      
         
    </>
  )
}

export default ProductScreen;
