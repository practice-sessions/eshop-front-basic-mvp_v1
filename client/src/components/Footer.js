import React from 'react';

const Footer = () => {
  return (
    <div className='container'>
      <footer className="bg-primary text-white mt-5 p-4 text-center">
        Copyright &copy; {new Date().getFullYear()} eShopFront Basic MVP1
      </footer>
    </div>
  )
}

export default Footer;
