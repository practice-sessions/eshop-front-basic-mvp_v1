import React from 'react';
import { Link } from 'react-router-dom';

const Product = ({ product }) => {
  return (
    
    <div className='row'>
      <div className='col s12 m4 l12'>
        <div className='card'>

          <div className='card-image waves-effect waves-block waves-light'> 
            <Link to={`/product/${product._id}`}>
              <img className='responsive-img' src={product.image} alt={product.description} />
            </Link> 
          </div>
        
          <div className='card'>
            <div className="card-content">
              <div className='container'>
                
                <span className='card-title grey-text text-darken-4'>
                  <Link to={`/product/${product._id}`}>
                    <strong>{product.name}</strong>
                  </Link>
                </span>
                
                <span className='grey-text text-darken-4'>
                  <strong>{product.rating} from {product.numReviews} reviews</strong>
                </span>
                
                <p>
                <span className='card-title black-text text-darken-4'>
                  <Link to={`/product/${product._id}`}>
                    <strong>£{product.price}</strong>
                  </Link>
                </span>
                </p>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Product;
