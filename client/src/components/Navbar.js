import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <Fragment>
      <header>
        
        <nav className='black'>
          <div className='container'>
          <div className='nav-wrapper'>
            <Link to='/' className='brand-logo'>eShopFront Basic MVP1</Link>
            <Link to='#!' data-target='mobile-demo' className='sidenav-trigger'><i className='material-icons'>menu</i></Link>
            <ul id='nav-mobile' className='right hide-on-med-and-down'>
              <li><Link to='/search'>Search</Link></li>
              <li><Link to='/cart'><i className='fas fa-shopping-cart'></i>{' '}Cart</Link></li>
              <li><Link to='/login'><i className='fas fa-user'></i>{' '}Sign In</Link></li>
            </ul>
          </div>
          </div>
        </nav>

        {/* May need a container class, though unlikely! */}
        <ul className='sidenav' id='mobile-demo'>
          <li><Link to='search'>Search</Link></li>
          <li><Link to='/cart'>Cart</Link></li>
          <li><Link to='/login'>Sign In</Link></li>
        </ul>
        
    </header>
    </Fragment>
  )
}

export default Navbar;
